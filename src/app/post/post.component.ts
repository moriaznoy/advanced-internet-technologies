import { PostsService } from './../posts.service';
import { Posts } from './../interface/posts';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-posts',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  panelOpenState = false;
 

  Posts$: Posts[];


  constructor(private postsrvice: PostsService) { }

  ngOnInit() {
    return this.postsrvice.getPost()
    .subscribe(data =>this.Posts$ = data ); 
  }

}
